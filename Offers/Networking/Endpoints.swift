//
//  Endpoints.swift
//  Offers
//
//  Created by Fournier Pierre on 05/10/2021.
//

import Foundation

enum EndPoints: String {
    case Offers = "https://raw.githubusercontent.com/leboncoin/paperclip/master/listing.json"
    case Categories = "https://raw.githubusercontent.com/leboncoin/paperclip/master/categories.json"
}
