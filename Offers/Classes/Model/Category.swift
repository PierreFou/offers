//
//  Category.swift
//  Offers
//
//  Created by Fournier Pierre on 07/10/2021.
//

import Foundation

struct Category: Decodable, Encodable {
    static let kAllCategoryId = -1
    
    let id: Int
    let name: String
}
