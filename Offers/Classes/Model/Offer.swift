//
//  Offer.swift
//  Offers
//
//  Created by Fournier Pierre on 05/10/2021.
//

import Foundation

struct Offer: Decodable {
    let id: Int
    let categoryId: Int
    let title: String
    let description: String
    let price: Double
    let imagesUrl: ImageUrl
    let creationDate: Date
    let isUrgent: Bool
    let siret: String?
    
    var category: String? {
        CategoryCoordinator.shared.categoryName(forId: categoryId)
    }
}

extension Array where Element == Offer {
    /// Ordered by Urgent first, and then CreationDate from the newest to the oldest
    var sort: [Offer] {
        var urgentOffers = [Offer]()
        var nonUrgentOffers = [Offer]()
        
        self.forEach { offer in
            offer.isUrgent ? urgentOffers.append(offer) : nonUrgentOffers.append(offer)
        }
        
        urgentOffers.sort(by: { $0.creationDate > $1.creationDate })
        nonUrgentOffers.sort(by: { $0.creationDate > $1.creationDate })
        
        return urgentOffers + nonUrgentOffers
    }
}
