//
//  ImageUrl.swift
//  Offers
//
//  Created by Fournier Pierre on 06/10/2021.
//

import Foundation

struct ImageUrl: Decodable {
    let small: String?
    let thumb: String?
}
