//
//  CategoryCoordinator.swift
//  Offers
//
//  Created by Fournier Pierre on 07/10/2021.
//

import Foundation

final class CategoryCoordinator {
    
    static let shared = CategoryCoordinator()
    
    var categories: [Category]?
    
    func categoryName(forId id: Int) -> String? {
        categories?.first(where: { $0.id == id})?.name
    }
}
