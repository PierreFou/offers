//
//  CategoryService.swift
//  Offers
//
//  Created by Fournier Pierre on 07/10/2021.
//

import Foundation

enum CategoryService {

    static func getCategories(completion: @escaping (Result<[Category], APIServiceError>) -> Void) {
        guard let url = URL(string: EndPoints.Categories.rawValue) else {
            return completion(.failure(.invalidUrl))
        }

        URLSession.shared.resumeDataTask(with: url, withTypedResponse: completion)
    }
}
