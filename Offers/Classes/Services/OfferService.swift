//
//  OfferService.swift
//  Offers
//
//  Created by Fournier Pierre on 05/10/2021.
//

import Foundation

enum OfferService {

    static func getOffers(completion: @escaping (Result<[Offer], APIServiceError>) -> Void) {
        guard let url = URL(string: EndPoints.Offers.rawValue) else {
            return completion(.failure(.invalidUrl))
        }

        URLSession.shared.resumeDataTask(with: url, withTypedResponse: completion)
    }
}
