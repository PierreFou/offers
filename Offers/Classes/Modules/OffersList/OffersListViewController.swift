//
//  OffersListViewController.swift
//  Offers
//
//  Created by Fournier Pierre on 05/10/2021.
//

import UIKit

class OffersListViewController: UIViewController {
    
    // MARK: - UI
    fileprivate var filterView = UIView()
    fileprivate var filterHeaderLabel = UILabel()
    fileprivate var filterButton = UIButton()
    fileprivate var offersTableView = UITableView()
    fileprivate let loaderView = UIActivityIndicatorView(style: .gray)
    
    // MARK: - properties
    private var viewModel: OffersListViewModel!
    private var categorySelected: Category? {
        didSet {
            viewModel.selectCategory(categorySelected?.id)
            if let category = categorySelected {
                let filterAttributedString = NSAttributedString(string: category.name,
                                                                attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue])
                filterButton.setAttributedTitle(filterAttributedString, for: .normal)
            }
        }
    }
    
    // MARK: - constants
    private static let kCellIdentifier = String(describing: OfferTableViewCell.self)
    private static let kRowHeight: CGFloat = 120

    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = []
        view.backgroundColor = .groupTableViewBackground
        navigationItem.title = "offer.title".localized
        
        view.addSubview(setupLoader())
        view.addSubview(setupCategoryView())
        view.addSubview(setupTableView())
        
        setupConstraints()
        
        loaderView.startAnimating()
                
        self.viewModel = OffersListViewModel(reloadData)
    }

}

fileprivate extension OffersListViewController {
    
    func reloadData() {
        loaderView.stopAnimating()
        filterView.isHidden = false
        offersTableView.isHidden = false
        offersTableView.reloadData()
    }
    
    func setupLoader() -> UIView {
        loaderView.center = self.view.center
        return loaderView
    }
    
    func setupCategoryView() -> UIView {
        filterHeaderLabel = UILabel()
        filterHeaderLabel.text = "filter.selected".localized
        
        let filterAttributedString = NSAttributedString(string: "filter.all".localized,
                                                        attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue])
        filterButton = UIButton()
        filterButton.addTarget(self, action: #selector(showCategories(sender:)), for: .touchUpInside)
        filterButton.setTitleColor(.blue, for: .normal)
        filterButton.setAttributedTitle(filterAttributedString, for: .normal)
        
        filterView = UIView()
        filterView.isHidden = true
        filterView.addSubview(filterHeaderLabel)
        filterView.addSubview(filterButton)
        
        filterHeaderLabel.translatesAutoresizingMaskIntoConstraints = false
        filterButton.translatesAutoresizingMaskIntoConstraints = false
        // Filter subviews
        NSLayoutConstraint.activate([
            // Header Label
            filterHeaderLabel.leadingAnchor.constraint(equalTo: filterView.leadingAnchor, constant: .small),
            filterHeaderLabel.topAnchor.constraint(equalTo: filterView.topAnchor),
            filterHeaderLabel.bottomAnchor.constraint(equalTo: filterView.bottomAnchor),
            // Button
            filterButton.leadingAnchor.constraint(equalTo: filterHeaderLabel.trailingAnchor),
            filterButton.topAnchor.constraint(equalTo: filterView.topAnchor),
            filterButton.bottomAnchor.constraint(equalTo: filterView.bottomAnchor)
        ])
        
        return filterView
    }
    
    func setupTableView() -> UITableView {
        offersTableView.isHidden = true
        offersTableView.delegate = self
        offersTableView.dataSource = self
        offersTableView.register(OfferTableViewCell.self, forCellReuseIdentifier: OffersListViewController.kCellIdentifier)
        offersTableView.rowHeight = OffersListViewController.kRowHeight
        
        return offersTableView
    }
    
    func setupConstraints() {
        filterView.translatesAutoresizingMaskIntoConstraints = false
        offersTableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            // Filter view
            filterView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            filterView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            filterView.heightAnchor.constraint(equalToConstant: 30),
            filterView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            // Offers table view
            offersTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            offersTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            offersTableView.topAnchor.constraint(equalTo: filterView.bottomAnchor),
            offersTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    @objc func showCategories(sender: UIButton) {
        let categoriesViewController = CategoriesListViewController()
        categoriesViewController.categorySelected = { category in
            self.categorySelected = category
        }
        present(UINavigationController(rootViewController: categoriesViewController), animated: true)
    }
}

extension OffersListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.filteredOffers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: OffersListViewController.kCellIdentifier) as? OfferTableViewCell else {
            fatalError()
        }
        cell.setup(viewModel.filteredOffers[indexPath.row])
        
        return cell
    }
    
}

extension OffersListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        present(UINavigationController(rootViewController: OfferDetailViewController(offer: viewModel.filteredOffers[indexPath.row])), animated: true)
    }
}
