//
//  OfferTableViewCell.swift
//  Offers
//
//  Created by Fournier Pierre on 07/10/2021.
//

import UIKit

class OfferTableViewCell: UITableViewCell {
    
    // UI
    fileprivate var previewImageView = UIImageView()
    fileprivate var titleLabel = UILabel()
    fileprivate var categoryLabel = UILabel()
    fileprivate var priceLabel = UILabel()
    fileprivate var creationDateLabel = UILabel()
    fileprivate var urgentImageView = UIImageView()
    fileprivate var bottomDateUrgentStackView = UIStackView()
    fileprivate var infosStackView = UIStackView()
    
    private static let kPlaceholderImageName = "offerPlaceholder"
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupImageView()
        
        setupFonts()
        
        bottomDateUrgentStackView.distribution = .fillProportionally
        bottomDateUrgentStackView.addArrangedSubview(creationDateLabel)
        
        infosStackView.axis = .vertical
        infosStackView.distribution = .fillEqually
        [titleLabel, categoryLabel, priceLabel, bottomDateUrgentStackView].forEach {
            infosStackView.addArrangedSubview($0)
        }
        
        contentView.addSubview(previewImageView)
        contentView.addSubview(infosStackView)
        
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setup(_ offer: Offer) {
        titleLabel.text = offer.title
        
        if let category = offer.category {
            categoryLabel.text = category
        }
        
        priceLabel.text = offer.price.toPrice
        
        creationDateLabel.text = offer.creationDate.toString
        
        previewImageView.image = UIImage(named: OfferTableViewCell.kPlaceholderImageName)
        if let imageUrl = offer.imagesUrl.small {
            previewImageView.imageFromURL(imageUrl)
        }
        
        urgentImageView.contentMode = .scaleAspectFit
        urgentImageView.image = UIImage(named: "star")
        bottomDateUrgentStackView.addArrangedSubview(urgentImageView)
        urgentImageView.isHidden = !offer.isUrgent
    }
    
}

fileprivate extension OfferTableViewCell {
    func setupConstraints() {
        infosStackView.translatesAutoresizingMaskIntoConstraints = false
        previewImageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            // Image view
            previewImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .small),
            contentView.bottomAnchor.constraint(equalTo: previewImageView.bottomAnchor, constant: .small),
            previewImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .small),
            previewImageView.widthAnchor.constraint(equalToConstant: 100),
            // Infos stack view
            infosStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .small),
            infosStackView.leadingAnchor.constraint(equalTo: previewImageView.trailingAnchor, constant: .small),
            contentView.bottomAnchor.constraint(equalTo: infosStackView.bottomAnchor, constant: .small),
            contentView.trailingAnchor.constraint(equalTo: infosStackView.trailingAnchor, constant: .small)
        ])
    }
    
    func setupImageView() {
        previewImageView.layer.cornerRadius = 10
        previewImageView.layer.masksToBounds = true
        previewImageView.contentMode = .scaleAspectFill
    }
    
    func setupFonts() {
        titleLabel.font = UIFont(name: .fontBold, size: 16.0)
        [categoryLabel, priceLabel, creationDateLabel].forEach {
            $0.font = UIFont(name: .font, size: 15.0)
        }
    }
}
