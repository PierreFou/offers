//
//  OffersListViewModel.swift
//  Offers
//
//  Created by Fournier Pierre on 05/10/2021.
//

import Foundation

final class OffersListViewModel {
    
    private var updateOffers: (() -> Void)
    
    private(set) var fetchedOffers = [Offer]() {
        didSet {
            filteredOffers = fetchedOffers
        }
    }
    private(set) var filteredOffers = [Offer]() {
        didSet {
            updateOffers()
        }
    }
    
    private func fetchOffers() {
        OfferService.getOffers { (result: Result<[Offer], APIServiceError>) in
            switch result {
            case .success(let offers):
                DispatchQueue.main.async {
                    self.fetchedOffers = offers.sort
                }
                break
            case .failure(let error):
                print("Error: \(error)")
                break
            }
        }
    }
    
    private func fetchCategories() {
        CategoryService.getCategories { (result: Result<[Category], APIServiceError>) in
            switch result {
            case .success(let categories):
                DispatchQueue.main.async {
                    CategoryCoordinator.shared.categories = categories
                    self.fetchOffers()
                }
                break
            case .failure(let error):
                print("Error: \(error)")
                break
            }
        }
    }
    
    func selectCategory(_ id: Int?) {
        guard let id = id, id != Category.kAllCategoryId else {
            filteredOffers = fetchedOffers
            return
        }
        filteredOffers = fetchedOffers.filter({ $0.categoryId == id })
    }
    
    init(_ completion: @escaping (() -> Void)) {
        updateOffers = completion
        fetchCategories()
    }
}
