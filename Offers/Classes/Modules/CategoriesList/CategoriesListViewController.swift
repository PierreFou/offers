//
//  CategoriesListViewController.swift
//  Offers
//
//  Created by Fournier Pierre on 08/10/2021.
//

import UIKit

class CategoriesListViewController: UIViewController {

    // MARK: - UI
    fileprivate var categoriesTableView = UITableView()
    
    // MARK: - properties
    fileprivate var viewModel: CategoriesListViewModel!
    
    private static let kCellIdentifier = String(describing: CategoryTableViewCell.self)
    private static let kRowHeight: CGFloat = 50
    
    var categorySelected: ((Category) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addCloseButton()
        navigationItem.title = "categories.title".localized
        
        view.addSubview(setupTableView())

        self.viewModel = CategoriesListViewModel()
    }

}

fileprivate extension CategoriesListViewController {
    
    func setupTableView() -> UITableView {
        categoriesTableView = UITableView(frame: view.bounds)
        categoriesTableView.delegate = self
        categoriesTableView.dataSource = self
        categoriesTableView.register(CategoryTableViewCell.self, forCellReuseIdentifier: CategoriesListViewController.kCellIdentifier)
        categoriesTableView.rowHeight = CategoriesListViewController.kRowHeight
        
        return categoriesTableView
    }
}

extension CategoriesListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoriesListViewController.kCellIdentifier) as? CategoryTableViewCell else {
            fatalError()
        }
        cell.setup(viewModel.categories[indexPath.row].name)
        
        return cell
    }
}

extension CategoriesListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        categorySelected?(viewModel.categories[indexPath.row])
        dismiss(animated: true)
    }
}
