//
//  CategoriesListViewModel.swift
//  Offers
//
//  Created by Fournier Pierre on 08/10/2021.
//

import Foundation

final class CategoriesListViewModel {
    
    private(set) var categories: [Category]
    
    init() {
        let allCategory = Category(id: Category.kAllCategoryId, name: "filter.all".localized)
        var categories = CategoryCoordinator.shared.categories ?? [Category]()
        categories.insert(allCategory, at: 0)
        
        self.categories = categories
    }
}
