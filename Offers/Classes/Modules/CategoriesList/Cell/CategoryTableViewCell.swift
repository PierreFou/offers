//
//  CategoryTableViewCell.swift
//  Offers
//
//  Created by Fournier Pierre on 08/10/2021.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {
    
    fileprivate var categoryLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(categoryLabel)
        
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
    
    func setup(_ category: String) {
        categoryLabel.text = category
    }
    
}

fileprivate extension CategoryTableViewCell {
    func setupConstraints() {
        categoryLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            // Label
            categoryLabel.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: .medium),
            categoryLabel.trailingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.trailingAnchor, constant: .medium),
            categoryLabel.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor),
            categoryLabel.bottomAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
}
