//
//  OfferDetailViewController.swift
//  Offers
//
//  Created by Fournier Pierre on 08/10/2021.
//

import UIKit

class OfferDetailViewController: UIViewController {
    
    // MARK: - UI
    fileprivate let scrollView = UIScrollView()
    fileprivate var contentView = UIView()
    fileprivate var mainImageView = UIImageView()
    fileprivate var titleLabel = UILabel()
    fileprivate var descriptionLabel = UILabel()
    fileprivate var priceStackView = UIStackView()
    fileprivate var creationDateProSiretStackView = UIStackView()
    fileprivate var categoryLabel = UILabel()
    fileprivate var priceLabel = UILabel()
    fileprivate var creationDateLabel = UILabel()
    fileprivate var siretLabel = UILabel()
    fileprivate var urgentLabel = UILabel()
    fileprivate var urgentImageView = UIImageView()
    fileprivate var urgentStackView = UIStackView()
    
    // MARK: - private properties
    fileprivate var viewModel: OfferDetailViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        navigationItem.title = "detail.title".localized
        addCloseButton()
        
        setupFonts()
        
        setupImages()
        setupLabels()
        setupStakViews()
        
        contentView.addSubview(mainImageView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(priceStackView)
        contentView.addSubview(creationDateProSiretStackView)
        contentView.addSubview(categoryLabel)
        contentView.addSubview(descriptionLabel)
        
        scrollView.addSubview(contentView)
        
        view.addSubview(scrollView)
        
        setupConstraints()
    }

    convenience init(offer: Offer) {
        self.init(nibName: nil, bundle: nil)
        self.viewModel = OfferDetailViewModel(offer: offer)
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder) {
        self.init()
    }
    
}

fileprivate extension OfferDetailViewController {
    
    func setupImages() {
        if let imageUrl = viewModel.imageUrl {
            mainImageView.imageFromURL(imageUrl)
        }
        mainImageView.contentMode = .scaleAspectFit
        
        urgentImageView.contentMode = .scaleAspectFit
        urgentImageView.image = UIImage(named: "star")
    }
    
    func setupLabels() {
        titleLabel.text = viewModel.titleLabel
        titleLabel.numberOfLines = 0
        
        priceLabel.text = viewModel.priceLabel
        
        urgentLabel.text = viewModel.urgentLabel
        
        creationDateLabel.text = viewModel.creationDateLabel
        
        if let siretNumber = viewModel.siretLabel {
            siretLabel.text = siretNumber
        }
        
        categoryLabel.text = viewModel.categoryLabel
        
        descriptionLabel.text = viewModel.descriptionLabel
        descriptionLabel.numberOfLines = 0
    }
    
    func setupStakViews() {
        urgentStackView.addArrangedSubview(urgentLabel)
        urgentStackView.addArrangedSubview(urgentImageView)
        urgentStackView.distribution = .fillProportionally
        
        priceStackView.distribution = .equalCentering
        priceStackView.addArrangedSubview(priceLabel)
        if viewModel.isUrgent {
            priceStackView.addArrangedSubview(urgentStackView)
        }
        
        creationDateProSiretStackView.axis = .vertical
        creationDateProSiretStackView.spacing = .small
        creationDateProSiretStackView.addArrangedSubview(creationDateLabel)
        
        if viewModel.siretLabel != nil {
            creationDateProSiretStackView.addArrangedSubview(siretLabel)
        }
    }
    
    func setupConstraints() {
        [mainImageView, titleLabel, priceLabel,
         categoryLabel, descriptionLabel, urgentLabel,
         siretLabel, creationDateLabel, scrollView, urgentImageView, urgentStackView,
         priceStackView, creationDateProSiretStackView, contentView].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        NSLayoutConstraint.activate([
            // Scroll view
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            // Content view
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            contentView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            // Image view
            mainImageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            mainImageView.widthAnchor.constraint(equalTo: contentView.widthAnchor),
            mainImageView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            // Title label
            titleLabel.topAnchor.constraint(equalTo: mainImageView.bottomAnchor, constant: .medium),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .medium),
            contentView.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor, constant: .medium),
            // Urgent image view
            urgentImageView.widthAnchor.constraint(equalTo: urgentImageView.heightAnchor),
            // Urgent label
            urgentLabel.widthAnchor.constraint(equalToConstant: 70),
            // Price stack view
            priceStackView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: .small),
            priceStackView.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            priceStackView.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            priceStackView.heightAnchor.constraint(equalToConstant: .medium),
            // Creation date label
            creationDateProSiretStackView.topAnchor.constraint(equalTo: priceStackView.bottomAnchor, constant: .small),
            creationDateProSiretStackView.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            creationDateProSiretStackView.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            // Category label
            categoryLabel.topAnchor.constraint(equalTo: creationDateProSiretStackView.bottomAnchor, constant: .small),
            categoryLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            categoryLabel.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            // Description label
            descriptionLabel.topAnchor.constraint(equalTo: categoryLabel.bottomAnchor, constant: .small),
            descriptionLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            descriptionLabel.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            descriptionLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
    
    func setupFonts() {
        titleLabel.font = UIFont(name: .fontBold, size: 16.0)
        [categoryLabel, priceLabel, creationDateLabel,
         descriptionLabel, siretLabel, urgentLabel].forEach {
            $0.font = UIFont(name: .font, size: 15.0)
        }
    }
}
