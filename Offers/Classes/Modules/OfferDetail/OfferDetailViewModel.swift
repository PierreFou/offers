//
//  OfferDetailViewModel.swift
//  Offers
//
//  Created by Fournier Pierre on 08/10/2021.
//

import Foundation

final class OfferDetailViewModel {
    
    private let offer: Offer!
    
    var imageUrl: String? {
        offer.imagesUrl.thumb ?? offer.imagesUrl.small
    }
    
    var titleLabel: String {
        offer.title
    }
    
    var priceLabel: String {
        offer.price.toPrice
    }
    
    var creationDateLabel: String {
        String(format: "creationDate.text".localized, offer.creationDate.toString)
    }
    
    var descriptionLabel: String {
        offer.description
    }
    
    var isUrgent: Bool {
        offer.isUrgent
    }
    
    var urgentLabel: String {
        "offer.urgent".localized
    }
    
    var categoryLabel: String {
        CategoryCoordinator.shared.categoryName(forId: offer.categoryId) ?? "category.none".localized
    }
    
    var siretLabel: String? {
        guard let siret = offer.siret else { return nil }
        return String(format: "pro.siret".localized, siret)
    }
    
    init(offer: Offer) {
        self.offer = offer
    }
}
