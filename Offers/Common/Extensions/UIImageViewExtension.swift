//
//  UIImageViewExtension.swift
//  Offers
//
//  Created by Fournier Pierre on 07/10/2021.
//

import UIKit

extension UIImageView {
    
    func imageFromURL(_ URLString: String) {
        guard let imageUrl = URLString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
              let url = URL(string: imageUrl) else { return }
        
        URLSession.shared.dataTask(with: url, completionHandler: { (data, _, error) in
            
            DispatchQueue.main.async {
                guard error == nil, let data = data, let downloadedImage = UIImage(data: data) else { return }
                
                self.image = downloadedImage
            }
        }).resume()
    }
}
