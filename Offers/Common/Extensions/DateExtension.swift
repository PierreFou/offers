//
//  DateExtension.swift
//  Offers
//
//  Created by Fournier Pierre on 07/10/2021.
//

import Foundation

extension Date {
    var toString: String {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "fr_FR")
        formatter.dateFormat = "dd MMM yyyy"
        return formatter.string(from: self)
    }
}
