//
//  CGFloatExtension.swift
//  Offers
//
//  Created by Fournier Pierre on 08/10/2021.
//

import UIKit

extension CGFloat {
    static var small: CGFloat = 10
    static var medium: CGFloat = 20
}
