//
//  UIViewControllerExtension.swift
//  Offers
//
//  Created by Fournier Pierre on 08/10/2021.
//

import UIKit

extension UIViewController {
    func addCloseButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "close"),
                                                            style: .done,
                                                            target: self,
                                                            action: #selector(onClose))
    }

    @objc func onClose(){
        self.dismiss(animated: true, completion: nil)
    }
}
