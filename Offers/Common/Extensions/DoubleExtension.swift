//
//  DoubleExtension.swift
//  Offers
//
//  Created by Fournier Pierre on 07/10/2021.
//

import Foundation

extension Double {
    var toPrice: String {
        let priceStr = "\(self)"
        guard let indexToShortCurrency = priceStr.firstIndex(of: ".") else { return "\(priceStr) \(String.currency)" }
        
        return  "\(priceStr.prefix(upTo: indexToShortCurrency)) \(String.currency)"
    }
}
