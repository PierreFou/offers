//
//  StringExtension.swift
//  Offers
//
//  Created by Fournier Pierre on 07/10/2021.
//

import Foundation

extension String {
    static var currency = "€"
    static var font = "HelveticaNeue"
    static var fontBold = "HelveticaNeue-Bold"
    
    var localized: String {
        NSLocalizedString(self, comment: "")
    }
}
