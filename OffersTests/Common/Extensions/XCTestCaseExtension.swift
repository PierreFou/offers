//
//  XCTestCaseExtension.swift
//  OffersTests
//
//  Created by Fournier Pierre on 10/10/2021.
//

import XCTest
@testable import Offers

extension XCTestCase {
    
    func loadOfferStub() -> [Offer] {
        let data = try! Data(contentsOf: Bundle(for: type(of: self)).url(forResource: "Offers", withExtension: "json")!)
        
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        return try! decoder.decode([Offer].self, from: data)
    }
}
