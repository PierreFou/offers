//
//  OffersTests.swift
//  OffersTests
//
//  Created by Fournier Pierre on 05/10/2021.
//

import XCTest
@testable import Offers

class OffersTests: XCTestCase {
    
    var offers: [Offer]!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        // Load Stub
        self.offers = loadOfferStub()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testOfferDetailViewModel() throws {
        // Given an offer (mocked)
        // When I get the first one
        // Then detail view model about it is valid

        if let firstOffer = offers.first {
            // Initialize View Model
            let viewModel = OfferDetailViewModel(offer: firstOffer)
            
            XCTAssertNotNil(viewModel.imageUrl)
            XCTAssertNotNil(viewModel.titleLabel)
            XCTAssertNotNil(viewModel.priceLabel)
            XCTAssertNotNil(viewModel.creationDateLabel)
            XCTAssertNotNil(viewModel.descriptionLabel)
            XCTAssertFalse(viewModel.isUrgent)
            XCTAssertNotNil(viewModel.urgentLabel)
            XCTAssertNotNil(viewModel.categoryLabel)
            XCTAssertNil(viewModel.siretLabel)
        }
        
    }

    func testOfferOrder() throws {
        // Given all offers
        // When I sort them
        // Then they are ordered by urgent first, then by creation date
        
        let orderedOffers = offers.sort
        
        guard let firstOffer = orderedOffers.first,
              let lastOffer = orderedOffers.last,
              let firstNonUrgentOffer = orderedOffers.first(where: { !$0.isUrgent }),
              let lastUrgentOffer = orderedOffers.last(where: { $0.isUrgent }) else {
            XCTFail()
            return
        }
        
        // is urgent
        XCTAssertTrue(firstOffer.isUrgent)
        XCTAssertFalse(lastOffer.isUrgent)
        
        // Last urgent is older than firt urgent
        XCTAssertTrue(firstOffer.creationDate > lastUrgentOffer.creationDate)
        XCTAssertTrue(firstNonUrgentOffer.creationDate > lastOffer.creationDate)
    }

}
